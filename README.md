### tieBaApp

前端：Vue cli3 + TypeScript

后端：nodeJs + Express `待重构成TS版本`

数据库：mysql，使用 [knex](<https://github.com/tgriesser/knex>)

UI部分使用 [element-ui](<https://github.com/ElemeFE/element>) ，简单完成贴吧功能（注册、登录、关注贴吧、回复帖子）。



### 使用注意
```
1、修改server/db.js 数据库连接

2、在mysql中执行mqcode.sql文件，刷新数据库

3、展示部分 npm run start; 后台部分 npm run server


```
### Next 功能
- 前端优化和叠加功能
- 后端使用TS


`By Author Lance`